/**
 * Client side code.
 */
(function() {
    "use strict";
    angular.module("userRegApp").controller("userRegCtrl", userRegCtrl);

    userRegCtrl.$inject = ["$http"];

    function userRegCtrl($http) {
        var self = this;

        self.user = {
            email: "",
            password: "",
            fullname: "",
            gender: "",
            dateOfBirth: "",
            address: "",
            country: "",
            contactNo: ""
        };

        self.displayUser = {
            email: "",
            password: "",
            fullname: "",
            gender: "",
            dateOfBirth: "",
            address: "",
            country: "",
            contactNo: ""
        };

        self.ageValid = function(dateOfBirth) {            
            var minAge = 18;
             console.log ("dateofbirth>>>" + dateOfBirth);
             var ageDifMs = Date.now() - dateOfBirth.getTime();
             var ageDate = new Date(ageDifMs); // miliseconds from epoch
             var ageYear = Math.abs(ageDate.getUTCFullYear() - 1970);
             
            console.log(ageYear);
             if(ageYear >= minAge){
                 return true;
             } else {
                 return false;
             }
         };

        self.registerUser = function() {
            console.log(self.user.email);
            console.log(self.user.password);
            console.log(self.user.fullname);
            console.log(self.user.gender);
            console.log(self.user.dateOfBirth);
            console.log(self.user.address);
            console.log(self.user.country);
            console.log(self.user.contactNo);
            $http.post("/users", self.user)
                .then(function(result) {
                    console.log(result);
                    self.displayUser.email = result.data.email;
                    self.displayUser.password = result.data.password;
                    self.displayUser.fullname = result.data.fullname;
                    self.displayUser.gender = result.data.gender;
                    self.displayUser.dateOfBirth = result.data.dateOfBirth;
                    self.displayUser.address = result.data.address;
                    self.displayUser.country = result.data.country;
                    self.displayUser.contactNo = result.data.contactNo;
                }).catch(function(error) {
                    console.log(error);
                });
        };
    }
}) ();