/**
 * Server side code.
 */
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3000;

app.use(express.static(__dirname + "/../client/"));

app.post("/users", function(req, res) {
    console.log("User information received: " + JSON.stringify(req.body));
    var user = req.body;
    user.email = user.email;
    user.password = user.password;
    user.fullname = user.fullname;
    user.gender = user.gender;
    user.dateOfBirth = user.dateOfBirth;
    user.address = user.address;
    user.country = user.country;
    user.contactNo = user.contactNo;
    console.log("Email: " + user.email);
    console.log("Password: " + user.password);
    console.log("Full Name: " + user.fullname);
    console.log("Gender: " + user.gender);
    console.log("D.O.B: " + user.dateOfBirth);
    console.log("Address: " + user.address);
    console.log("Country: " + user.country);
    console.log("Contact No: " + user.contactNo);
    res.status(200).json(user);
});

app.use(function(req, res) {
    res.send("<h1>!!!! Page not found ! ! !</h1>");
});

app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});

module.exports = app;